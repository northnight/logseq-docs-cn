* 你可以将块及其子块转为模板以便重用。
* 比如，下面我们将制作一个会议笔记模板块来重用。
** 会议名称
:PROPERTIES:
:作者: logseq
:项目: 待填写
:标签: 待填写
:template: 会议 
:END:
*** <%today%>
*** [[会议任务]]
*** 讨论
*** 该项目相关笔记
*** {{query (property project change-me-later)}}
* 两种方式将上面的块及其子块转为模板：
** 你可以右击块旁圆点，点击“制作模板”后取一个名字。
[[https://logseq.github.io/assets/template_1621928689810_0.gif]]
** 你也可以给块直接添加 ~template~ 属性，并附模板名。
[[https://logseq.github.io/assets/template2_1621928922947_0.gif]]
* 使用模板时，只需键入 ~/Template~ 命令并选择模板名:
[[https://logseq.github.io/assets/template3_1621929392325_0.gif]]
* 其他关于如何使用模板的教程
** [[Cobblebot]] 贡献的精彩 [[https://discuss.logseq.com/t/templates-how-to-create-edit-and-insert/200][论坛教程]]
** [[OneStutteringMind]] 的精彩视频也展示了如何使用模板(注意，视频中的属性语法已经废弃，查看 [[术语/属性]] 中的最新语法)
[[https://logseq.github.io/#/page/templates]]
* [[动态变量]]支持
** *注意：* [[宏]]同样支持动态变量
** *语法：* ~<% 某值 %>~
** *支持的变量：*
*** ~today~ => ~[[今日日志页面]]~
*** ~yesterday~ => ~[[昨日日志页面]]~
*** ~tomorrow~ => ~[[明日日志页面]]~
*** ~time~ => ~当前时间~，如 22:44
*** ~current page~ => ~[[当前页面]]~
*** 自然语言日期
~Last Friday~ => [[Feb 12th, 2021]]
****
#+BEGIN_QUOTE
- Today,Tomorrow,Yesterday,Last Friday,等等
- 17 August 2013 - 19 August 2013
- This Friday from 13:00 - 16.00
- 5 days ago
- 2 weeks from now
- Sat Aug 17 2013 18:40:39 GMT+0900 (JST)
- 2014-11-30T08:15:30-05:30
#+END_QUOTE
范例来自 [[https://github.com/wanasit/chrono]]
* ~ inculuding-parent::false~ 模板属性指明是否包含当前块模板的顶级内容
** 举例：
:PROPERTIES:
:id: 61322a1e-46f2-4420-84cc-0dcd08d4a283
:END:
*** 我默认被包含在模板示例1中
:PROPERTIES:
:template: 模板示例1 
:END:
**** 第1行
***** 第二行
*** 由于包含父级项的设置，我没有被包含
:PROPERTIES:
:template: 模板示例2
:including-parent: false 
:END: