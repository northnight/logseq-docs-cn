* Logseq 使用酷炫的[[https://revealjs.com/][Reveal.js]]构建幻灯片功能。
背景图片：[[https://images.unsplash.com/photo-1498855926480-d98e83099315]]
* 如何使用？
** 在某个非日志页面，点击文件链接旁的幻灯图标，它将在右侧边栏打开当前页面的幻灯片。
** 按 ~f~ 在全屏模式下打开幻灯片。
* 问题
** 如何为块设置背景图片？
*** 添加 ~background-image~ 属性。 #块属性
:PROPERTIES:
:background-image: https://images.unsplash.com/photo-1498855926480-d98e83099315 
:END:
** 如何设置其他属性？
*** 使用 ~xyz~ 替换属性名 ~data-xyz~